<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Upload;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $builder
            ->add('imageName')
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Image',
                'allow_delete' => true,
                'download_uri' => false,
                'image_uri' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Upload'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) : void
    {
        $resolver->setDefaults([
            'data_class' => Upload::class,
        ]);
    }
}