<?php

namespace App\Message;

class TestNotification
{
    private $test;

    public function __construct(string $test)
    {
        $this->test = $test;
    }

    /**
     * @return string
     */
    public function getTest(): string
    {
        return $this->test;
    }
}