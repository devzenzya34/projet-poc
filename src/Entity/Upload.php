<?php

namespace App\Entity;

use App\Repository\UploadRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


#[ORM\Entity(repositoryClass: UploadRepository::class)]
/**
 * @Vich\Uploadable
 */
class Upload
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $imageName;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="upload", fileNameProperty="imageName")
     *
     * @var File|null
     */
    private $imageFile;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'uploads')]
    private $uploadBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     * @return Upload
     */
    public function setImageFile(?File $imageFile): Upload
    {
        $this->imageFile = $imageFile;

        if(null !== $imageFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
        return $this;
    }

    public function getUploadBy(): ?User
    {
        return $this->uploadBy;
    }

    public function setUploadBy(?User $uploadBy): self
    {
        $this->uploadBy = $uploadBy;

        return $this;
    }


}
