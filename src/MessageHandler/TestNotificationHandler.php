<?php


namespace App\MessageHandler;


use App\Message\TestNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class TestNotificationHandler implements MessageHandlerInterface
{
    public function __invoke(TestNotification $message)
    {
//        dd($message->getTest());
        return $message->getTest();
    }

}